var libs = {
    portal: require('/lib/xp/portal'),
    content: require('/lib/xp/content'),
    thymeleaf: require('/lib/thymeleaf'),
    optionlink: require('/lib/openxp/optionlink')
};

exports.macro = function (context) {


    var params = context.params;

    if (!params.optionlink) return;

    var content = libs.content.get({key:params.optionlink});

    if (!content || !content.data || !content.data.optionlink) return;

    var optionlink = content.data.optionlink;

    var model = {
      optionlink: libs.optionlink.getLink(optionlink)
    };

    if (params.text!==undefined && params.text !== ''){
      model.optionlink.text = params.text;
    }
    var view = resolve('/site/fragments/openxp/optionlink.html');
    var html = libs.thymeleaf.render(view, model);

    return {
        contentType:'text/html',
        body: html
    }
};
