var libs = {
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/thymeleaf'),
    content: require('/lib/xp/content'),
    optionlink: require('/lib/openxp/optionlink')
};

function handleGet(req) {
    var content = libs.portal.getContent();
    if (content.type !== app.name + ':optionlink') return;
    return handleOptionLink(req, content);
}

var handleOptionLink = function(req, content){

    var optionlink = content.data.optionlink;

    if (!optionlink) return;

    var model = {
        optionlink: libs.optionlink.getLink(optionlink)
    };

    var view = resolve('optionlink.html');
    var html = libs.thymeleaf.render(resolve('/site/fragments/openxp/optionlink.html'), model);

    return {
        body: html
    };
};

exports.get = handleGet;
