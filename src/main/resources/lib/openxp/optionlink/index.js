var libs = {
    portal: require('/lib/xp/portal'),
    content: require('/lib/xp/content')
};

/**
* @typedef {Object} linkobject
* @property {string} url The link url
* @property {string} target The link _target attribute
* @property {string} text The text of the link (default:displayName)
*/

/**
* @typedef {Object} optionlink
* @property {string} Expects a https://bitbucket.org/openxp/optionlink mixin json object
*/

/**
* Expects a https://bitbucket.org/openxp/optionlink mixin javascript object and
* returns a javascript object with href-url, href-target and link text.
* @param {optionlink}
* @return {linkobject}
*/
exports.getLink = function(optionlink){
    if (optionlink === undefined){
        return null;
    }
    var link = getLinkOfType(optionlink);
    return link;
};

function getLinkOfType(optionlink) {
    if (optionlink._selected === 'content') {
        return getContentLink(optionlink.content);
    }
    else if (optionlink._selected === 'url') {
        return getUrlLink(optionlink.url);
    }
    else if (optionlink._selected === 'email') {
        return getEmailLink(optionlink.email);
    }
    else if (optionlink._selected === 'attachment') {
        return getAttachmentLink(optionlink.attachment)
    }
    return {};
}

function getUrlLink(urlLink){
    return {
        url: urlLink.url,
        target: urlLink.target  || '_self',
        text: urlLink.text ? urlLink.text : urlLink.url,
        class: (urlLink.class ? urlLink.class : '') + ' optionlink-url'
    }
}

function getContentLink(contentLink) {
    var text = contentLink.text;

    if (text===undefined){
      text = getContentDisplayName(contentLink.content);
    }
    var url = libs.portal.pageUrl({
      id:contentLink.content,
      type: contentLink.type || 'server'});

    url = appendUrlParams(url, contentLink.params);

    return {
        url: url,
        target: contentLink.target || '_self',
        text: text,
        class: (contentLink.class ? contentLink.class : '') + ' optionlink-content'
    }
}

function getContentDisplayName(contentKey){
  try{
    if (contentKey !== undefined){
      return libs.content.get({key: contentKey}).displayName;
    }
  }catch(e){
    log.warning("Could not get fallback displayName for optionlink without text");
  }
}

function getAttachmentLink(attachmentLink) {
    var text = attachmentLink.text;
    if (text===undefined){
      text = getContentDisplayName(attachmentLink.attachment);
    }

    var url = libs.portal.attachmentUrl({
      id: attachmentLink.attachment,
      download: attachmentLink.download || true,
      type: attachmentLink.type || 'server'});

      url = appendUrlParams(url, attachmentLink.params);

    return {
        url: url,
        target: attachmentLink.target  || '_self',
        text: text,
        class: (attachmentLink.class ? attachmentLink.class : '') + ' optionlink-attachment'
    }
}

function getEmailLink(emailLink) {
    var url = '';
    if (emailLink.email) {
        url = 'mailto:'+emailLink.email+'?';
        if (emailLink.cc) {
            url += 'cc='+emailLink.cc+'&';
        }
        if (emailLink.bcc) {
            url += 'bcc='+emailLink.bcc+'&';
        }
        if (emailLink.subject) {
            url += 'subject='+emailLink.subject+'&';
        }
        if (emailLink.body) {
            url += 'body='+emailLink.body;
        }
    }
    return {
        url: url,
        text: emailLink.text,
        class: (emailLink.class ? emailLink.class : '') + ' optionlink-email'
    };
}

function appendUrlParams(url, params){
  if (url !== undefined && params !== undefined){
    try{
      if (url.indexOf('?')===-1 && params.indexOf('?')!==-1){//this is expected behavior
        return url + params;
      }
      else if (url.indexOf('?')!==-1 && params.indexOf('?')!==-1){
        return url + params.replace('?', '&');
      }else if (params !== undefined){
        return url + params;
      }
    }catch(e){
      log.warning("Exception while trying to append params '" + params + "' to url: " + url);
    }
  }
  return url;
}
