![Scheme](application.png)

# Optionlink app / lib for Enonic XP

---
**Important build info**
Existing builds with dependency from maven url https://dl.bintray.com/openxp/public will break as bintray services are deprecated on May 1st 2021. Fix this by replacing maven url with following https://openxp.jfrog.io/artifactory/public.

---

App / lib with two optionlink mixins, one macro, one contenttype and a javascript library to insert HtmlArea style links in contenttypes and parts.

## Releases and Compatibility

See [info about releases on Enonic Market](https://market.enonic.com/vendors/rune-forberg/optionlink)

## Note for versions >= 2.0.0

Previously Optionlink could only be included as a lib inside another app, this is still possible, and preferable if you want to have link inputs inside your content-types, part- or page-configs.

Since version 2.0.0 you may also use Optionlink as an app. This can perhaps be useful if
you would like to create a link to a content, but include parameters (this is not possible with HtmlAreas standard link functionality), or if you for other reasons would like you link to be a separate content.

For versions lower then 2.0.0 maven url should be `https://repo.enonic.com/public` instead of jfrog and namespace should be `openxp.lib` instead of openxp.app.

## Warning on using Optionlink as app / lib at the same time

You may not use Optionlink as a standalone app and a library in an app at the same site at the same time. This will cause naming collisions as Content Studio only allows one content-type with the name "Optionlink", regardless of different app namespace. If you have previously used optionlink as an app, but wants to start using it as a library inside another app instead, you may export existing Optionlink content with Data Toolbox, search/replace
namespace openxp.app.optionlink:optionlink to com.app.yourapp:optionlink, then re-import.

## Usage

Add jfrog repository to the repository list in the build.gradle file:

    repositories {
        maven {
            url 'https://openxp.jfrog.io/artifactory/public'
        }
    }

After this, add the following dependency:

    dependencies {
        include 'openxp.app:optionlink:2.0.0'
    }


## Description

Optionlink mixin has four options:

1. contentLink (with parameters, anchor and other options)
2. attachmentLink
3. urlLink
4. emailLink (with link-text, cc, bcc, subject, body)

Links support params, anchor, relative/absolute url, content-disposition for
attachments, link target etc.

## Optionlink contenttype

This library includes a Optionlink contenttype, which can be used to create
a Optionlink content that can be used in other parts - and it can also be
added as a macro in a HtmlArea. This can be used to achieve links to other
content with added parameters or anchor.

## OptionlinkSelector macro

This enables you insert a macro inside a HtmlArea and select a Optionlink contenttype 
which is rendered by using the included fragment /site/fragments/openxp/optionlink.html. 
The optionlink text can be overridden for each macro that is inserted.

Example:
    [optionlinkselector text="" optionlink="784ed301-b4df-482f-a1e7-11c520e3accd"/]

## Mixins

There are two mixins included. The latter has no separate input for link text,
but it will make available the target content displayName as a fallback text.
Useful if you are creating a clickable call-to-action type box without its own
link text.

    <mixin name="optionlink"/>
    <mixin name="optionlink-without-text"/>

## Usage in controller

    var optionlink = require('/lib/openxp/optionlink');
    model.link = libs.optionlink.getLink(optionlink);

    getLink(optionlink) returns an object with three properties:
    * @property {string} url - The link url
    * @property {string} target - The link _target attribute
    * @property {string} text - The text of the link (default:displayName)

## Example Usage in view

    <a data-th-replace="/site/fragments/openxp/optionlink.html :: process(${link})"/>

## Built in contenttype optionlink.xml

  Library includes a simple optionlink contenttype that can be rendered as described above.

## Include an optionlink in a contenttype

    <content-type>
        <display-name>Optionlink</display-name>
        <super-type>base:structured</super-type>
        <form>
            <mixin name="optionlink"/>
        </form>
    </content-type>

    <item-set name="optionlinks">
        <label>Optionlinks</label>
        <items>
            <mixin name="optionlink"/>
        </items>
        <occurrences minimum="0" maximum="0"/>
    </item-set>

## Usage an optionlink in part config

    <part>
        <display-name>Part with optionlink</display-name>
        <form>
            <mixin name="optionlink"/>
        </form>
    </part>
